from asyncio import subprocess
import PySimpleGUI as sg
import iuliia
from ldap3 import Server, Connection, NTLM, ALL
from ldap3.extend.microsoft.addMembersToGroups import ad_add_members_to_groups as addUsersInGroups
from ldap3 import MODIFY_ADD, MODIFY_REPLACE, MODIFY_DELETE
import os
import time

def _onKeyRelease(event):
    sg.set_options(suppress_raise_key_errors=False, suppress_error_popups=False, suppress_key_guessing=False)
    ctrl  = (event.state & 0x4) != 0
    if event.keycode==88 and  ctrl and event.keysym.lower() != "x":
        event.widget.event_generate("<<Cut>>")

    if event.keycode==86 and  ctrl and event.keysym.lower() != "v":
        event.widget.event_generate("<<Paste>>")

    if event.keycode==67 and  ctrl and event.keysym.lower() != "c":
        event.widget.event_generate("<<Copy>>")

sg.theme('DefaultNoMoreNagging')   # Add a touch of color
# All the stuff inside your window.

layout = [  [sg.Text('Добаляем пользователя в AD')],
            [sg.Text('Фамилия', size=(15, 1)), sg.InputText(expand_x=True)], #0
            [sg.Text('Имя', size=(15, 1)), sg.InputText(expand_x=True)], #1
            [sg.Text('Компания', size=(15, 1)), sg.InputText(expand_x=True)],#2
            [sg.Text('Должность', size=(15, 1)), sg.InputText(expand_x=True)],#3           
            [sg.Text('Отдел / Подразделение', size=(15, 1)), sg.InputText(expand_x=True)],#4          
            [sg.Text('Телефон(короткий)', size=(15, 1)), sg.InputText(expand_x=True)],#5
            [sg.Text('Мобильный', size=(15, 1)), sg.InputText(expand_x=True)],#6
            [sg.Text('Комната', size=(15, 1)), sg.InputText(expand_x=True)],#7            
            [sg.Text('Добавляем пользователя в группы:')],
            [sg.Checkbox('Group1')],#8
            [sg.Checkbox('Group2')],#9          
            [sg.Button('Ok', size=(15, 1)), sg.Button('Cancel')] ]

# Create the Window
window = sg.Window("Road to HEll", layout, finalize=True)
window.TKroot.bind_all("<Key>", _onKeyRelease, "+")
# Event Loop to process "events" and get the "values" of the inputs
while True:
    event, values = window.read()
    if event == sg.WIN_CLOSED or event == 'Cancel': # if user closes window or clicks cancel
        break
    AD_SERVER = 'YOU_SERVER_IP'
    AD_USER = 'YOU_AD_USER'
    AD_PASSWORD = 'YOU_AD_PASSWORD'
    AD_SEARCH_TREE = 'DC=LOCAL,DC=LOCAL'
        
    usersOU = 'OU=Users,OU=LOCAL,DC=LOCAL,DC=LOCAL'
    groupsOU = 'OU=Groups,OU=LOCAL,DC=LOCAL,DC=LOCAL'
        
    server = Server(AD_SERVER, use_ssl=True, port=636)
    conn = Connection(server, AD_USER , AD_PASSWORD, authentication=NTLM, auto_bind=True)
    dn = "DC=LOCAL,DC=LOCAL"
#conn.add('OU=USERS,OU=!TEST_OU,OU=MO NSO,DC=egisznso,DC=ru', 'organizationalUnit')
    name = values[1].strip().title() #Имя
    sname = values[0].strip().title() #Фамилия
    fname = sname + ' ' + name #отображаемое имя
    source_name = name + ' ' + sname
    translited_name = source_name[0].lower() + source_name.split()[1].lower()
    tname = iuliia.translate(translited_name, schema=iuliia.GOST_779_ALT)
    fio = tname.strip()
    departament = values[4].strip().title()  #Отдел / Подразделение
    company = values[2].strip()  #Компания
    telephoneNumber = values[5].strip() #Телефон(короткий)
    mobile = values[6].strip()  #Мобильный
    physicalDeliveryOfficeName = values[7].strip()  #Комната
    title = values[3].strip().title()  #Должность
    Group1 = values[8] # рассылка гип
    Group2 = values[9] # рассылка гис
#    homeDrive = 'Z:'
#    homeDirectory = (r"\\fs/Users/"+ tname).replace('/', '\\')
    userpswd = 'As123456#'

    if telephoneNumber == '':
        telephoneNumber = ' '
    if mobile == '':
        mobile = ' '
    
    conn.add('cn=' + fname + ',OU=Users,OU=LOCAL,DC=LOCAL,DC=LOCAL', 'user', {'sAMAccountName': tname, 'userPrincipalName': tname,'givenName': name, 'sn': sname, 'displayName': fname, 'mail': tname + '@gc-gip.ru', 'department' : departament, 'company': company, 'telephoneNumber': telephoneNumber, 'mobile': mobile, 'physicalDeliveryOfficeName': physicalDeliveryOfficeName, 'title': title})

    print(conn.result)

# enable user
    conn.extend.microsoft.unlock_account(
            user='cn=' + fname + ',OU=Users,OU=LOCAL,DC=LOCAL,DC=LOCAL')
    print(conn.result)
    
 # Устанавливаем пароль
    conn.extend.microsoft.modify_password(user='cn=' + fname + ',OU=Users,OU=LOCAL,DC=LOCAL,DC=LOCAL',
                                              new_password=userpswd, old_password=None)
    print(conn.result)

# enable user
    conn.modify(
    f"CN={fname},OU=Users,OU=LOCAL,DC=LOCAL,DC=LOCAL",
    {"userAccountControl": [(MODIFY_REPLACE, [66048])]},
)
    if Group1 == True:
        group_dn = "CN=Group1,OU=Groups,OU=LOCAL,DC=LOCAL,DC=LOCAL"
        conn.modify(group_dn, {'member': (MODIFY_ADD, ['cn=' + fname + ',OU=Users,OU=LOCAL,DC=LOCAL,DC=LOCAL'])})
    else:
        group_dn = "CN=Group2,OU=Groups,OU=LOCAL,DC=LOCAL,DC=LOCAL"
        conn.modify(group_dn, {'member': (MODIFY_ADD, ['cn=' + fname + ',OU=Users,OU=LOCAL,DC=LOCAL,DC=LOCAL'])})

    group_dn2 = "CN=AllUsers,OU=LOCAL,DC=LOCAL,DC=LOCAL"
    conn.modify(group_dn2, {'member': (MODIFY_ADD, ['cn=' + fname + ',OU=Users,OU=LOCAL,DC=LOCAL,DC=LOCAL'])})

    print(conn.result)
    
  #  sg.Print('Креды', do_not_reroute_stdout=False)
    print('Login:', tname)
    print('Pass:', userpswd )
       
    # Создаем папку пользователя
    my_file = open(tname + ".ps1", "w+")
    my_file.write('''$samAccountName = "'''+ tname +'''"
$fullPath = '''r'''"\\server\Users\{0}" -f $samAccountName
$driveLetter = "Z:"
 
$User = Get-ADUser -Identity $samAccountName
 
if($User -ne $Null) {
    Set-ADUser $User -HomeDrive $driveLetter -HomeDirectory $fullPath -ea Stop
    $homeShare = New-Item -path $fullPath -ItemType Directory -force -ea Stop
 
    $acl = Get-Acl $homeShare
 
    $FileSystemRights = [System.Security.AccessControl.FileSystemRights]"Modify"
    $AccessControlType = [System.Security.AccessControl.AccessControlType]::Allow
    $InheritanceFlags = [System.Security.AccessControl.InheritanceFlags]"ContainerInherit, ObjectInherit"        
    $PropagationFlags = [System.Security.AccessControl.PropagationFlags]"NoPropagateInherit"
 
    $AccessRule = New-Object System.Security.AccessControl.FileSystemAccessRule ($User.SID, $FileSystemRights, $InheritanceFlags, $PropagationFlags, $AccessControlType)        
    $acl.AddAccessRule($AccessRule)        
    Set-Acl -Path $homeShare -AclObject $acl -ea Stop
 
    Write-Host ("HomeDirectory created at {0}" -f $fullPath)}''')
    my_file.close()  
    
    print('Скрипт для папки создан')
    
    sg.popup_ok('Login:', tname, 'Pass:', userpswd)
window.close()
